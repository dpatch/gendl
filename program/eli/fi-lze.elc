;ELC   
;;; Compiled by layer@THOR on Wed Jul 11 14:42:23 2012
;;; from file c:/src/scm/acl90.32/src/cl/src/eli/fi-lze.el
;;; in Emacs version 24.1.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\301\300!\210\302\211\207" [fi::show-compilation-status make-variable-buffer-local nil] 3)
(defvar fi::mode-line-note-for-compile " COMPILING")
(defvar fi::mode-line-note-for-eval " EVALUATING")
(defalias 'fi::note-background-request #[(compiling) "\203 \306\202	 \307\203 	\202 \n\303\310\f\"\210\311\236\211\f\203+ \fA@\312\230\203> \f\2038 \fC\241\204> \311DB)\313!\211	*\207" [compiling fi::mode-line-note-for-compile fi::mode-line-note-for-eval message message1 minor-mode-alist "Compiling" "Evaluating" "%s..." fi::show-compilation-status "" fi::connection-process item fi::*connection*] 4])
(defalias 'fi::note-background-reply #[(&optional compiling) "\205 @\203 \305\202 \306\203 \301\307	\"\210\304\n\236\211\203% \310C\241\210)\311\211)\207" [compiling message minor-mode-alist item fi::show-compilation-status "Compiling" "Evaluating" "%s...done." "" nil] 4])
#@275 *This variable controls whether or not the fi:lisp-eval-or-compile-*
functions will compile or evaluate their forms.  If non-nil, then
compilation is the default, otherwise evaluation is the default.
The non-default functionality can be selected by using a prefix argument.
(defvar fi:lisp-evals-always-compile t (#$ . -1427))
(defalias 'fi::decode-prefix-argument-for-eval-or-compile #[nil "\203	 	?\202\n 	C\207" [current-prefix-arg fi:lisp-evals-always-compile] 1])
#@216 Send the current top-level (or nearest previous) form to the Lisp
subprocess associated with this buffer.  A `top-level' form is one that
starts in column 1.  See the documentation for
fi:lisp-evals-always-compile.
(defalias 'fi:lisp-eval-or-compile-defun #[(compilep) "\203 \301 \207\302 \207" [compilep fi:lisp-compile-defun fi:lisp-eval-defun] 1 (#$ . 1904) (fi::decode-prefix-argument-for-eval-or-compile)])
#@210 Send the text in the region to the Lisp subprocess associated with this
buffer, one expression at a time if there is more than one complete
expression.  See the documentation for fi:lisp-evals-always-compile.
(defalias 'fi:lisp-eval-or-compile-region #[(compilep) "\203 \301 \207\302 \207" [compilep fi:lisp-compile-region fi:lisp-eval-region] 1 (#$ . 2325) (fi::decode-prefix-argument-for-eval-or-compile)])
#@141 Send the sexp before the point to the Lisp subprocess associated with
this buffer.  See the documentation for fi:lisp-evals-always-compile.
(defalias 'fi:lisp-eval-or-compile-last-sexp #[(compilep) "\203 \301 \207\302 \207" [compilep fi:lisp-compile-last-sexp fi:lisp-eval-last-sexp] 1 (#$ . 2743) (fi::decode-prefix-argument-for-eval-or-compile)])
#@133 Send the entire buffer to the Lisp subprocess associated with this
buffer.  See the documentation for fi:lisp-evals-always-compile.
(defalias 'fi:lisp-eval-or-compile-current-buffer #[(compilep) "\203 \301 \207\302 \207" [compilep fi:lisp-compile-current-buffer fi:lisp-eval-current-buffer] 1 (#$ . 3101) (fi::decode-prefix-argument-for-eval-or-compile)])
#@174 Send for evaluation the current top-level (or nearest previous) form to
the Lisp subprocess associated with this buffer.  A `top-level' form is one
that starts in column 1.
(defalias 'fi:lisp-eval-defun #[nil "\212\302 \210`)\212\303 \210`)\304	\305#*\207" [end start end-of-defun fi:beginning-of-defun fi::eval-region-internal nil] 4 (#$ . 3466) nil])
#@175 Send for compilation the current top-level (or nearest previous) form to
the Lisp subprocess associated with this buffer.  A `top-level' form is one
that starts in column 1.
(defalias 'fi:lisp-compile-defun #[nil "\212\302 \210`)\212\303 \210`)\304	\305#*\207" [end start end-of-defun fi:beginning-of-defun fi::eval-region-internal t] 4 (#$ . 3829) nil])
#@84 Send for evaluation the region to the Lisp subprocess associated with
this buffer.
(defalias 'fi:lisp-eval-region #[nil "\300`\301 ^`\301 ]\302#\207" [fi::eval-region-internal fi::mark nil] 4 (#$ . 4193) nil])
#@85 Send for compilation the region to the Lisp subprocess associated with
this buffer.
(defalias 'fi:lisp-compile-region #[nil "\300`\301 ^`\301 ]\302#\207" [fi::eval-region-internal fi::mark t] 4 (#$ . 4409) nil])
#@99 Send for evaluation the sexp before the point to the Lisp subprocess
associated with this buffer.
(defalias 'fi:lisp-eval-last-sexp #[nil "\212\301\302!\210`)\303`\304#)\207" [start forward-sexp -1 fi::eval-region-internal nil] 4 (#$ . 4627) nil])
#@100 Send for compilation the sexp before the point to the Lisp subprocess
associated with this buffer.
(defalias 'fi:lisp-compile-last-sexp #[nil "\212\301\302!\210`)\303`\304#)\207" [start forward-sexp -1 fi::eval-region-internal t] 4 (#$ . 4884) nil])
#@91 Send for evaluation the entire buffer to the Lisp subprocess associated
with this buffer.
(defalias 'fi:lisp-eval-current-buffer #[nil "\300ed\301\302$\207" [fi::eval-region-internal nil t] 5 (#$ . 5142) nil])
#@92 Send for compilation the entire buffer to the Lisp subprocess associated
with this buffer.
(defalias 'fi:lisp-compile-current-buffer #[nil "\300ed\301\211$\207" [fi::eval-region-internal t] 5 (#$ . 5358) nil])
(defalias 'fi::eval-region-internal #[(start end compilep &optional ignore-package) "\306!\210p\307\310\311\312\n\313\314\f{!\315\316\f]d=\205! \f^e=?\205& \317\320 \321\205/ \311\322@\323=\257\324	E\325	E&)\207" [compilep buffer fi:emacs-to-lisp-transaction-directory start end fi:echo-evals-from-buffer-in-listener-p fi::note-background-request lep::send-request-in-new-session lep::evaluation-request t :transaction-directory :text fi::defontify-string :echo :partialp :pathname buffer-file-name :compilep :return-string minibuffer #[(results stuff buffer compilep) "\212q\210	@\306=\203' \n;\203 \nG\307U\203 \310C!\210\202, \311\312\n\"\210\202, \310C!\210\f;\203: \204: \311\312\f\"\210)\f\205G \313!\210db\207" [buffer fi:pop-up-temp-window-behavior stuff compilep results fi:echo-evals-from-buffer-in-listener-p minibuffer 0 fi::note-background-reply fi:show-some-text nil pop-to-buffer fi:pop-to-sublisp-buffer-after-lisp-eval fi:common-lisp-buffer-name] 3] #[(error buffer compilep) "\212q\210\303	C!\210\304\305	\203 \306\202 \307\n#)\207" [buffer compilep error fi::note-background-reply message "Error during %s: %s" "compile" "eval"] 4] fi:pop-up-temp-window-behavior ignore-package] 18])
